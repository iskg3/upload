.EXPORT_ALL_VARIABLES:

IMAGE_NAME ?= upload
IMAGE_TAG ?= latest

VAULT_ADDR = https://vault.mst.edu

login:
	vault login -method=oidc -no-print

logout:
	rm -f $(HOME)/.vault-token

test:
	vault kv list secret/k8s

all:
	sed -e s/__PARENT_BRANCH__/main/g Dockerfile.tmpl > Dockerfile
	docker build --no-cache=true -t upload .
	rm -f Dockerfile

buildkit:
	sed -e s/__PARENT_BRANCH__/main/g Dockerfile.tmpl > Dockerfile
	DOCKER_BUILDKIT=1 docker build --no-cache=true -t upload .
	rm -f Dockerfile

clean:
	rm -rf buildtmp
	-docker rmi -f $(IMAGE_NAME):latest
	-docker rmi -f $(IMAGE_NAME):$(IMAGE_TAG)
	-docker stop $(CONTAINER_NAME)
	-docker rm $(CONTAINER_NAME)

image:
	@echo "Working $(IMAGE_NAME):$(IMAGE_TAG) which will publish to $(DKR_REPO):$(REPO_TAG)"
	docker build -t $(IMAGE_NAME):$(IMAGE_TAG) ./.devcontainer
	docker image history $(IMAGE_NAME):$(IMAGE_TAG)