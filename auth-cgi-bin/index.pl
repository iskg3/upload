#!/usr/bin/perl

# Begin-Doc
###############################################################################
# Name: index.pl
# Type: Perl Script
#
# Description display main page
#
# Functions: none
#
# Tables: none
###############################################################################
# End-Doc

use lib "/local/perllib/libs";
use lib "/local/mstperl/libs";
use Local::HTMLUtil;
use JSON;
use MST::AppTemplate;
use Text::ParseWords;

use lib "/local/upload/libs";
use upload;
#use misctools;
use strict;

my $html = new MST::AppTemplate(
    title => "Upload the CSV files"
);

my $debug_mode = 1;

$html->PageHeader();
HTMLGetRequest();

# Print title
print "CSV files into tables with filtering"; #, $ENV{HOSTNAME};
print "<br/>";

# Setting the Required priviledge - latter can be per menu
$html->RequirePriv("itapps:lwi:menu");

my $len_rqpairs = scalar keys %rqpairs;
my $mode = lc $rqpairs{'mode'};

if ($debug_mode) {
    print "rgpairs_len = ", $len_rqpairs;
    print "<br/>";
    print "All the request pairs: ";
    print "@{[ %rqpairs ]}";
    print "<br/>";
}
my $script_url = HTMLScriptURL();
if ((!$mode) or ($mode eq 'home')) { # Menu if mode is empty
    HTMLStartForm($script_url);
    HTMLSubmit( "LIST", 'mode' );
    print "&nbsp;" x 18;
    print "<b>List</b> the uploaded CSV file with some filtering<br /><br />";
    HTMLEndForm();

    HTMLStartForm($script_url);
    HTMLSubmit( "UPLOAD", 'mode' );
    print "&nbsp;" x 10;
    print "<b>Upload</b> the CSV file to database<br /><br />";
    HTMLEndForm();
} elsif ($mode eq 'list') { # List the uploaded file with some filtering
    HTMLStartMultiForm();
    print "Select CSV File for list: ";
    HTMLInputFile('infile', 20, '', 200);
    print "Search String: ";
    HTMLInputText('txtFilter', 20, '', 255);
    HTMLHidden('mode','view');
    HTMLSubmit('Submit');
    HTMLEndForm();
} elsif ($mode eq 'upload') { # Upload file to database
    HTMLStartMultiForm();
    print "Select CSV File for download: ";
    HTMLInputFile('infile', 20, '', 200);
    HTMLHidden('mode','result');
    HTMLSubmit('Submit');
    HTMLEndForm();
} elsif ($mode eq 'view') {
    print "VIEW";
    print "<br/>";
    upload_file();
    HTMLStartForm($script_url);
    HTMLSubmit( "HOME", 'mode' );
    HTMLEndForm();
} elsif ($mode eq 'result') {
    print "RESULT";
    print "<br/>";
    upload_to_db();
    HTMLStartForm($script_url);
    HTMLSubmit( "HOME", 'mode' );
    HTMLEndForm();
} else {
    print "Out of mode";
}

#print "<pre>", $json->pretty->encode(\%ENV), "</pre>"; 
#display_header();
#display_footer();

$html->PageFooter();
