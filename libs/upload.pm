use MST::MySQLObject;

sub upload_to_db {
   my $infile     = $rqpairs{'infile'}; 
   if ($debug_mode) {
        print "rgpairs_len = ", $len_rqpairs;
        print "<br/>";
        print "@{[ %rqpairs ]}";
        print "<br/>";
        print "Mode: ", $mode;
    }

    if ( !$infile ) {
        $html->ErrorExit("**No file was selected**");
    }

    if ( substr( $infile, -4, 4 ) ne '.csv' ) {
        $html->ErrorExit("**The selected file is not CSV format. Upload has failed**");
    }

    my $fhr = HTMLGetFile('infile')
        or $html->ErrorExit("**Cannot get handler for the selected file. Upload has failed**");

    my $fin_buffer = '';
    my $chunksize  = 4000;
    my $buffer;
    while ( my $bytesread = read( $fhr, $buffer, $chunksize ) ) {
        $fin_buffer .= $buffer;
    }

    print "Reading file Ok";

    my $mysqldb = new MST::MySQLObject;
    $mysqldb->SQL_OpenDatabase("sys*")
        or $html->ErrorExitSQL( 'Cant open database server', $mysqldb );

    my $list_qry = "select *
                    from rawdata_uploads";

    my $list_qry_cid = $mysqldb->SQL_OpenQuery($list_qry)
        or $html->ErrorExitSQL( $list_qry, $mysqldb );
    
    print "DB Ok";

}

sub upload_file {
    my $infile     = $rqpairs{'infile'};
    my $txtFilter = $rqpairs{'txtFilter'};

    if ($debug_mode) {
        print "rgpairs_len = ", $len_rqpairs;
        print "<br/>";
        print "@{[ %rqpairs ]}";
        print "<br/>";
        print "Mode: ", $mode;
    }

    if ( !$infile ) {
        $html->ErrorExit("**No file was selected**");
    }
    if ( substr( $infile, -4, 4 ) ne '.csv' ) {
        $html->ErrorExit("**The selected file is not CSV format. Upload has failed**");
    }

    my $fhr = HTMLGetFile('infile')
        or $html->ErrorExit("**Cannot get handler for the selected file. Upload has failed**");

    my $fin_buffer = '';
    my $chunksize  = 4000;
    my $buffer;
    while ( my $bytesread = read( $fhr, $buffer, $chunksize ) ) {
        $fin_buffer .= $buffer;
    }

    my @data_list = split '\n', $fin_buffer;
    
    #print "<table>" . "\n";
    print "<table>";
    foreach my $each_list (@data_list) {
        #print "Row ";
        print "<tr>"; 
        if (!$txtFilter) { # No search text, print all rows
            print_data($each_list);
        } elsif (index($each_list, $txtFilter) != -1) {
            print_data($each_list); 
        }
        print "</tr>";

    }
    print "</table>";
}

sub print_data {
    my $each_list = $_[0];
    my @eachlist = quotewords( ',', 0, $each_list );
    foreach (@eachlist) {
        print "<td>" . "$_" . "</td>";
    }
    #print "<br/>";  
}

1;