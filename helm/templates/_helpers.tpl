# Default values for helm.
# This is a YAML-formatted file.
# Declare variables to be passed into your templates.

{{- define "helm.default_namespace" -}}
{{-   if .Values.namespace }}
{{-     .Values.namespace }}
{{-   else }}
{{-     if eq .Release.Namespace "default"  }}
{{-       "" | required "must specify --namespace, default not permitted" }}
{{-     else }}
{{-       .Release.Namespace }}
{{-     end }}
{{-   end }}
{{- end }}
