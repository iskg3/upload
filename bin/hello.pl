#!/usr/bin/perl

# Begin-Doc
# Name: hello.pl
# Description: test script for the autocron
# End-Doc

use lib "/local/perllib/libs";
use lib "/local/mstperl/libs";
use Sys::Hostname;
use Local::Env;
use Local::CurrentUser;

use strict;

$| = 1;

my $env = Local_Env();
my $userid   = lc Local_CurrentUser();
my $hostname = lc hostname();

my $shorthost = $hostname;
$shorthost =~ s/\..*//go;

my $cluster = $hostname;
$cluster =~ s/\..*//go;
$cluster =~ s/-[a-z]+[0-9]+$//;

if ( $ENV{AUTOCRON_CLUSTER} ne "" ) {
    $cluster = $ENV{AUTOCRON_CLUSTER};
}

print "USERID: $userid\n";
print "SHORT HOST: $shorthost\n";
print "CLUSTER: $cluster\n";
print "LOCAL ENV: $env\n";

