Dummy/stub application account for orientation of new developers to our containerized workflow, kubernetes environment, and shared pipelines.

[[_TOC_]]


# Software Requirements
You will need to install the following software on your system
1. Docker (19.03.0+)/Docker Desktop
1. git
1. OpenSSH (ssh, ssh-agent, etc.)
   * For windows: ensure you have at least feature pack 1903
   * Go to Settings -> Apps -> Optional features -> Add a feature -> Install both "OpenSSH Client" and "OpenSSH Server"
   * You may need to go into your system services and ensure the ssh-agent starts automatically
1. [kubectl](https://kubernetes.io/docs/tasks/tools/install-kubectl/)
1. VS Code (optional but recommended)
   * You need to install the "Remote Development" extension pack.
1. If you are using windows, make sure you have PowerShell installed

# Configuration
1. Setup git:
   ```
   git config --global user.name "Your Name"
   git config --global user.email youremail@mst.edu
   ```
1. Setup SSH
   1. Generate your keypair (recommended that you use a passphrase)
      ```
      ssh-keygen -t rsa-sha2-512 -b 4096 -C youremail@mst.edu
      ```
   1. Add your public key to [gitlab](https://git.mst.edu/profile/keys)
1. Setup docker by logging into our private container registry (use your username/password):
   ```
   docker login git-docker.mst.edu
   ```
1. Setup kubectl
   1. Login as `SSO@umsystem.edu` in [Platform9](https://mst.platform9.net)
   1. Click cluster and download kubeconfig of k8s-app-dev. Please make sure to choose 'Token' in the authentication method.
   1. Run the following command
      ```export KUBECONFIG=[Location of Kubconfig file]/k8s-apps-dev.yaml```
   1. Verify the kubeconfig is valid:
      ```
      $ kubectl config get-contexts
      CURRENT   NAME               CLUSTER            AUTHINFO     NAMESPACE
      *         default            k8s-apps-dev       Sophia Kim   default
                k8s-apps-dev-pf9   k8s-apps-dev-pf9   Sophia Kim   default
      ```

# Development Workflow
## Standing up initial container
   1. Make sure your ssh agent/keys are loaded
      ```
      ssh-add -L
      The agent has no identities.
      ```
      if no keys are listed, go ahead and add them
      ```
      ssh-add
      Enter passphrase for C:\Users\yoursso/.ssh/id_rsa:
      Identity added: C:\Users\yoursso/.ssh/id_rsa (C:\Users\yoursso/.ssh/id_rsa)
      ```
   1. Open up a terminal (PowerShell if in Windows) and checkout the repository to a directory where you will be working on your code:
      ```
      git clone ssh://git@git.mst.edu/upload/upload
      ```
   1. If you are using VS Code
      1. Launch VS Code, press F1 and select "Remote-Containers: Open Folder in Container"
      1. Navigate/select the directory that you just cloned the source code to
      1. Click "Open"
         1. This will build a container
         1. name it `upload`
         1. Launch a webserver that you can access through: `https://localhost:8443`
         1. Injects any defined debuggers, ssh-agents, etc into the container
   1. If you want to use a different editor of choice:
      1. Open a terminal, navigate to the directory in question, and run `docker-compose up`
      1. This will build the same container, name it `upload` and launch a webserver that you can access through: `https://localhost:8443`
      1. Now you can access/edit the files directly with your editor of choice.
      1. When you're done run `docker-compose stop` in the same directory as the source code to stop the running container.
## Continued code changes
   1. Be sure to pull any possible changes from gitlab:
      ```
      git pull
      ```
   1. If you're using VS code: repeat step 3 from previous section.
   1. If you want to use a different editor of choice:
      1. Open a terminal, navigate to the directory in question, and run `docker-compose start`
      1. Repeat steps 4.2 through 4.4 of the last section

## Promoting code changes (CI/CD pipelines)
It's extremely simple to promote code changes to our kubernetes clusters. We still leverage our branched based environment model: main/test/production branches map to k8s-apps-dev/test/prod environments respectively. Any commits that get merged to these branches will get automatically picked up, built into a container, and then deployed to the appropriate environment based kubernetes cluster.

## Set up ArgoCD
ArgoCD application needs to be set up for the first time after the container image is created.
1. Log in to [ArgoCD](https://argocd.mst.edu)
1. Click +NEW APP
1. Click EDIT AS YAML
1. Copy the content of arcd/k8s-apps-dev-helm.yaml and paste to the editor
1. Click Save and then CREATE
1. It needs to be repeated for k8s-apps-test-helm.yaml and k8s-apps-helm.yaml

# Updating AuthSrv
1. Updating AuthSrv secrets, it needs to be updated in [vault.mst.edu](https://vault.mst.edu) and rebuild the container:

# Troubleshooting
The following are ways to troubleshoot a running pod in our kubernetes clusters using the commandline tool `kubectl`. You can do the same through the dashboard.
## Opening a shell
If you need to exec into a running pod (container) in our kubernetes cluster, you can do so as follows:
1. Get the name of the running pod
   ```
   $ kubectl get pods
   NAME                           READY   STATUS    RESTARTS   AGE
   upload-59c95bfc57-77lk4   1/1     Running   0          9h
   ```
1. Execute the following command
   ```
   kubectl exec -it upload-59c95bfc57-77lk4 -- bash
   ```
## Viewing logs
Using the name of the pod, you can get the logs as follows:
```
kubectl logs -f upload-59c95bfc57-77lk4
```

test